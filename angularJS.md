# what is digest cycle
Angular watches all its watchable object weather its value is changed or not. it is getting triggered after in some interval or some action on 
view side. The cycle is known as digest cycle.
We can mannulaly triggered it by `$scope.digest()` or `$scope.$apply()`

#what is diffrence between $apply and digest
$digest will get called internally from $apply. $apply will resolve all the $digest cycle and its associated watches for change. 
When you call $digest it will resolve watch from specific scope which you have called.
# what is directive. create a custom directive
 directives are markers on a DOM element (such as an attribute, element name, comment or CSS class) 
 that tell AngularJS's HTML compiler ($compile) to attach a specified behavior to that DOM element 
 (e.g. via event listeners), or even to transform the DOM element and its children.
 there are inbuilt directive as well e.g. ng-if, ng-repeat etc.
```
 var mainApp = angular.module("mainApp", []);
 mainApp.directive('student', function() {
    return {
        restrict = 'EA',
        template = "Student: <b>{{student.name}}</b> , Roll No: <b>{{student.rollno}}</b>";
        scope: "="
    }
 });
 ``` 
# what is difference between ng-if and ng-show/ng-hide
`ng-if` removes item from DOM tree where `ng-show/ng-hide` just play with its display and visibility
# what is filter in angularJS. create a custom filter
Filters are used to change modify the data and can be clubbed in expression or directives using pipe character.

```
var mainApp = angular.module("mainApp", []);
 mainApp.filter('makeUppercase', function () {
  return function (item) {
    return item.toUpperCase();
  };
});
```
# what does ng-include do
you can include HTML content using the ng-include directive e.g. `<div ng-include="'myFile.htm'"></div>`
# what is difference between service and factory
Factory and Service both are singleton. 
Factory can return anything which can be a class(constructor function) , instance of class , string , number or boolean .If you 
return a constructor function, you can instantiate in your controller. 
 
But Service does not need to return anything.

# what is structure of your angularJS/angular application
Unlike other people I also use angularJS as MVA(Module View Anything) or MVVC(Module View View-controller). 
Since in my application my view can also have business logic.

I use module to fetch resources(data from api) and wrap into multiple layers

    1. http layer where all http methods(get,put,post etc are present)
    2. authentication layer for public/private apis based on tockens
    3. inteceptor/logger layer
    
Controllers where my business logic will be there to modify data fetced from my module or based on some action on view side.

View where my template/html will be residing.

# how you will communicate between two controllers.

I can communicate among controller using following ways.

1.  using window object(very bad way)
2.  using rootscope(bad way)
3.  using services and events(good way). 

I personally like service way more because of its moduler way and its simplicity.
