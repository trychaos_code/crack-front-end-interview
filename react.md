# create a class and function component using react
```
import React from 'react';
import PropTypes from 'prop-types';

class Toast extends React.Component {
	constructor () {
		super();
		this.state = {
			autoClose: this.props.autoClose
		};
		this.dismissToast = this.dismissToast.bind(this);
	}

	dismissToast () {
		console.log("Im in dissmiss tost");
		
	}

	render () {
		return (
			<div >
                Hi {this.state.autoClose}
                <button onClick={e=>this.dismissToast()}></button>
			</div>
		);
	}
}

Toast.propTypes = {
	autoClose: PropTypes.bool
};

Toast.defaultProps = {
	autoClose: true
};

export default Toast;
```
# How will you change state in react
using `setState` method we will pass whichever state we want to change e.g. `this.setState({autoClose:false})`

# what all lifecycle it has
In start first we have to instanciate to component so we have  `constructor` then it goes for `componentWillMount`
after that `render` happens then it goes for `componentDidMount`

After that it goes in three phases
1. Props changes:
    `componentWillRecieveProps` then it goes for `shouldComponentUpdate` where if we return true then component will update 
    els it wont. After it goes for `componentWillUpdate` then `render` and then `componentDidUpdate`
    
2. State changes:
    it goes for `componentWillUpdate` then `render` and then `componentDidUpdate`
    
    
3. Unmounting phase:
    it goes for `componentWillUnmount` since component will not be in memory so there is no such phases like componentDidUnmount
    

# what is difference between react-15 and react-16
1. New Core Architecture
2. Fragments & Strings:
   we can also return an array of elements, and string from component’s render method.
3. Server Side Rendering
4. Reduced File Size

# what is difference between react-16 and react-17

# what is difference between props and state
Props are passed to the child component but states are internal phase of component e.g.

I created a datePicker component I will pass starting date as props

and I will maintain state of component like if user clicks on year he will go to year view, if he clicks on date he will 
go to date view. basically value of state will change from time to time.

# in which lifecycle you will you will fetch data from server

It depends on the requirement. Basically my preference is I'll set my page with loader and render it with spinner 
and in `componentDidMount` phase I'll fetch data and will update state based on responce.

# how you will fetch data in react

previously I was using `blue-bird` or some other library but now a days I use `fetch` because it has advantage over other 
library which are using normal XHR in its core like default caching, or CORS(cross origin request) handling. 

If some browser dosen't support it I use polyfills for it.

# How did you used redux with react

I used redux at very simple laval like follows:
1.  wrap whole application in provider
2.  create diffrent reducers for diffrent component
3. I had created simple middleware and in createStore method we pass result of `combineReducers` as first parameters
 and middleWare as 2nd.
4. In different component we were dispaching events with action and payload and based on action type reducers were 
updating the state
   
# what is virtual DOM and what is its benefits.

In most of the library our js interects with actual DOM which always triggers browsers rtender method every time.
But in react we create a element in javascript memory which is called virtual DOM.
 
we update all change to that and when we are finished we 
add it to actual DOM which calls browser's render metrhod only once. 

# Did you got a chance to work on react native. How did you used it

Yea I had worked with it on small lavel. It's simple we create our application in normal react way and after we finished 
we put it in an react native envirnment where we get some aditional methods to intrect with device hardware/software e.g. 
camera(media), contacts, messages etc.

# What is prop drilling and context API

# What is redux selector

# what is immutable js

# How you use react with typescript

# what is useState, useEffect, useCallback, useMemoise, with examples

# What all ways you can stop reRendring of component

# Difference between Pure Component and imPure Component

# What is key in concept of rect component

# what is connect in react redux. What is mapStateToProps, mapDispatchToProps

# What is difference between redux and react-redux

# what is getDerivedStateFromProps, shouldComponentUpdate lifecycle.

# what is redux-thunk and redux-saga

# Benifit of using functional component over class component

# Why facebook switch to functional component